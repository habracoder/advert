<?php

namespace DefaultBundle\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WidgetController
 * @package DefaultBundle\Controller\App
 */
class WidgetController extends Controller
{
    /**
     * @Template()
     * @param Request $request
     * @return array
     */
    public function navbarAction(Request $request)
    {
        $menu = [
            [
                'title' => 'Главная',
                'path' => 'app_default',
                'routes' => ['app_default']
            ],
            [
                'title' => 'Обявления',
                'path' => 'app_advert_list',
                'routes' => ['app_advert_list']
            ],
            [
                'title' => 'Контакты',
                'path' => 'app_contacts',
                'routes' => ['app_contacts']
            ],
        ];

        foreach($menu as $key => $item) {
            if (in_array($request->attributes->get('_route'), $item['routes'])) {
                $menu[$key]['active'] = true;
                break;
            }
        }

        return [
            'items' => $menu
        ];
    }

    /**
     * @Template()
     */
    public function breadcrumbsAction()
    {
        return [];
    }
}
