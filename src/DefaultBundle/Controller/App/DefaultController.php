<?php

namespace DefaultBundle\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @package DefaultBundle\Controller\App
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="app_default")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/contacts", name="app_contacts")
     * @Template()
     */
    public function contactsAction()
    {
        return [
            'title' => 'Контакты'
        ];
    }
}
