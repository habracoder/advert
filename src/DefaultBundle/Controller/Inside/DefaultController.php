<?php

namespace DefaultBundle\Controller\Inside;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @package DefaultBundle\Controller\Inside
 * @Route("/inside")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="inside_default")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }
}
