<?php

namespace AdBundle\Form\App;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvertType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', [
                'label' => 'Заголовок'
            ])
            ->add('description', 'textarea', [
                'label' => 'Описание'
            ])
            ->add('images', 'collection', [
                'label' => 'Изображение',
                'type' => new AdvertImageType()
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdBundle\Entity\Advert'
        ));
    }

    public function getName()
    {
        return 'advert';
    }
}