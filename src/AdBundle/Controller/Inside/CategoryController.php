<?php

namespace AdBundle\Controller\Inside;

use AdBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CategoryController
 * @package AdBundle\Controller\Inside
 * @Route("/inside/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/list", name="inside_category_list")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $er = $em->getRepository('AdBundle:Category');

        $parent = $request->get('parent');

        return [
            'parentNode' => isset($parentNode) ? $parentNode : null,
            'categories' => $er->findBy(['parent' => $parent])
        ];
    }
}
