<?php

namespace AdBundle\Controller\Inside;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class AdvertController
 * @package AdBundle\Controller\Inside
 * @Route("/inside/advert")
 */
class AdvertController extends Controller
{
    /**
     * @Route("/list", name="inside_advert_list")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $er = $em->getRepository('AdBundle:Category');

        $category = $er->findOneBy(['id' => 15]);


        $em = $this->get('doctrine');
        $er = $em->getRepository('AdBundle:Advert');

        return [
            'adverts' => $er->findBy([], ['createdAt' => 'desc']),
            'category' => $category
        ];
    }

    /**
     * @Route("/show/id{id}", name="inside_advert_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->get('doctrine');
        $er = $em->getRepository('AdBundle:Advert');

        return [
            'advert' => $er->findOneBy(['id' => $id])
        ];
    }
}
