<?php

namespace AdBundle\Controller\App;

use AdBundle\Entity\Advert;
use AdBundle\Form\App\AdvertType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdvertController
 * @package AdBundle\Controller\App
 * @Route("/advert")
 */
class AdvertController extends Controller
{
    /**
     * @Route("/list", name="app_advert_list")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->get('doctrine');
        $er = $em->getRepository('AdBundle:Advert');

        return [
            'adverts' => $er->findBy([], ['createdAt' => 'desc'])
        ];
    }

    /**
     * @Route("/show/id{id}", name="app_advert_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->get('doctrine');
        $er = $em->getRepository('AdBundle:Advert');

        return [
            'advert' => $er->findOneBy(['id' => $id])
        ];
    }

    /**
     * @param Request $request
     * @Route("/create", name="app_advert_create")
     * @Template()
     * @return array
     */
    public function createAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $er = $em->getRepository('AdBundle:Advert');
        $advert = new Advert();

        $form = $this->generateCreateForm($advert);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {

                $em->persist($advert);
                $em->flush();

                return $this->redirectToRoute('app_advert_list');
            } else {

            }
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Advert $advert
     * @return \Symfony\Component\Form\Form
     */
    public function generateCreateForm(Advert $advert)
    {
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->createForm(new AdvertType(), $advert, [
            'action' => $this->generateUrl('app_advert_create'),
        ]);

        $form->add('submin', 'submit', [
            'label' => 'Добавить'
        ]);

        return $form;
    }
}
